﻿using System;

namespace whileDowhile
{
    internal class Program
    {
        static void Main(string[] args)
        {
            bool continueProgramSofwareExecution = true;

            while (continueProgramSofwareExecution == true)
            {
                Console.WriteLine("Do you wish to keep the software running?\n(1 if yes) (0 if no)");
                int keepGoing = Convert.ToInt16(Console.ReadLine());

                if (keepGoing == 1)
                    Console.WriteLine("Hello World! the software will keep running");
                else if (keepGoing == 0)
                {
                    Console.WriteLine("This is the last time the software is running");
                    continueProgramSofwareExecution = false;
                }
                else
                {
                    Console.WriteLine("Invalid input, Try again");
                }
            }
        }
    }
}

/*using System;

namespace whileDowhile
{
    internal class Program
    {
        static void Main(string[] args)
        {
            bool continueProgramSofwareExecution = false;

            do
            {
                Console.WriteLine("Do you wish to keep the software running?\n(1 if yes) (0 if no)");
                int keepGoing = Convert.ToInt16(Console.ReadLine());
                if (keepGoing == 1)
                {
                    Console.WriteLine("Hello World! the software will keep running");
                    continueProgramSofwareExecution = true;
                }
                else if (keepGoing == 0)
                {
                    Console.WriteLine("This is the last time the software is running");
                    continueProgramSofwareExecution = false;
                }
                else
                {
                    Console.WriteLine("Invalid input, Try again");
                }
            } while (continueProgramSofwareExecution == true);
        }
    }
}*/
