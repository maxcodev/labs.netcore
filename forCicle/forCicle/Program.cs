﻿using System;

namespace forCicle
{
    internal class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; /*Condicion inicial*/ i < 50;/*Limite de repeticiones*/ i=i+10 /*Incremento*/)
            {
                Console.WriteLine("Hello World! #{0}", i);
            }
        }
    }
}
