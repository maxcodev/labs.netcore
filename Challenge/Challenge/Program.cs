﻿using System;

namespace Challenge
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string rights = "   ©2021 Developed by Maxcodev  ";
            Console.WriteLine(rights.Trim().ToUpper() + "\nWelcome to Easy Sumatory\nNow please type the first number, you can use decimal with dot or comma (Example: 2,5)");
            float a = float.Parse(Console.ReadLine());
            Console.WriteLine("Now type the second Number");
            float b = float.Parse(Console.ReadLine());
            float result = a + b;
            Console.WriteLine("The result is: " + result);
            Console.WriteLine(result.ToString() + " es el resultado");
        }
    }
}