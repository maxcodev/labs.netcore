﻿using System;

namespace ifStatement
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int anyValue = 14;
            string message = "";

            if (anyValue == 7)
            {
                message = "OMG, it's a miracle any Value is 7";
                Console.WriteLine($"The answer is: {message}");
            }
            else if (anyValue == 14)
            {
                message = "Super!, the Doble Value is 14";
                Console.WriteLine($"The answer is: {message}");
            }
            else
            {
                message = "Whooops, the Value wasn't 7";
                Console.WriteLine($"The answer is: {message}");
                Console.Beep();
            }
        }
    }
}
