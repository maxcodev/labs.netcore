﻿using System;

namespace Calculator
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Calculator 2000\nPlease choose an option, enter a number...\n1.- Sum\n2.- Rest\n3.- Multiplication\n4.- Divition");
            int selection = int.Parse(Console.ReadLine());
            float n1, n2, result;

            if (selection < 1 || selection > 4)
            {
                Console.WriteLine($"Incorrect option, please choose a correct option");
                return;
            }

            Console.WriteLine("Write the first number: ");
            n1 = float.Parse(Console.ReadLine());

            Console.WriteLine("Write the second number: ");
            n2 = float.Parse(Console.ReadLine());

            if (selection == 1)
            {
                result = n1 + n2;
                Console.WriteLine($"The sum is: {result}");
            }
            else if (selection == 2)
            {
                result = n1 - n2;
                Console.WriteLine($"The rest is: {result}");
            }
            else if (selection == 3)
            {
                result = n1 * n2;
                Console.WriteLine($"The multiplication is: {result}");
            }
            else if (selection == 4)
            {
                result = n1 / n2;
                Console.WriteLine($"The divition is: {result}");
            }
            Console.WriteLine("Thanks for using Calculator 2000 :)");
        }
    }
}

/*using System;

public class calculatorSwitch
{
    public static void Main()
    {
        Console.WriteLine("Enter the selected soda: ");
        string caseSwitch = Console.ReadLine();

        switch (caseSwitch)
        {
            case "cola":
                Console.WriteLine("Cola soda - $2 USD");
                break;
            case "lime":
                Console.WriteLine("Lime soda - $1 USD");
                break;
            case "orange":
                Console.WriteLine("Orange soda - $1.5 USD");
                break;
            case "apple":
                Console.WriteLine("Apple soda - $1 USD");
                break;
            default:
                Console.WriteLine("ERROR: You did not select a soda or you entered an incorrect value.");
                break;
        }
    }
}*/
