﻿// See https://aka.ms/new-console-template for more information
using System;
namespace ArrayDemoProject
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            string[] coffeTypes;
            float[] coffePrices;

            coffeTypes = new string[] { "Expresso", "Largo", "Filtrado", "Latte" };
            coffePrices = new float[] { 1.2f, 1.4f, 5.0f, 5.5f };

            coffeTypes[1] = "Lungo"; //Cambiamos el valor asignado anteriormente en el Arreglo en el índice 1 (Largo)

            for (int i = 0; i < coffeTypes.Length; i++)
            Console.WriteLine(coffeTypes[i] + " Coffe $" + coffePrices[i]);
        }
    }
}