﻿using System;
/*
 *1.- Validar si es usuario existente o se debe registrar y generar un sistema de registro o login
 *2.- El programa debe ser capaz de darle la bienvenida a un usuario existente si en efecto existe
 *3.- El programa debe repetirse hasta que se registren las 10 personas
 */
namespace Restaurant10TablesReservationSystem
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] userName = new string [10] {"Migue", "", "", "", "", "", "", "", "", ""};
            int arrayCurrentIndex = 1;
            bool userType = true;
            Console.WriteLine("Welcome to the best restaurant in the world!");
            while (true)
            {
                Console.WriteLine("\nAre you a registered user?\ntrue, or write false to register");
                userType = Convert.ToBoolean(Console.ReadLine());
                if(userType == true)
                {
                    Console.WriteLine("Hello, you are a registered user, please enter your exact user name");
                    string userToSearch = Console.ReadLine();
                    Console.WriteLine("The user you searched is {0}", userToSearch);
                    int index = Array.IndexOf(userName, userToSearch);
                    if(index == -1)
                    {
                        Console.WriteLine("User not found, try again or register");
                    }
                    else
                    {
                        Console.WriteLine("Welcome {0} it's a pleasure to give you food ", userName[index]);
                    }
                }else if (userType == false)
                {
                    Console.WriteLine("Please write and remember your User Name");
                    userName[arrayCurrentIndex] = Console.ReadLine();
                    Console.WriteLine("Your User Has been saved successfully\n" +
                    "Your User Name is {0}", userName[arrayCurrentIndex]);
                    arrayCurrentIndex++;
                }
            }
        }
    }
}
