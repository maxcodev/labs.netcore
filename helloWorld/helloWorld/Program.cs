﻿using System;

namespace helloWorld
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello Maximo!");

            int height = 174;
            int age = 20;
            string name = "Maximo Sanchez";
            string information = "The requested information is: \nUser name " 
                + name + " \nUser age " + age + " \nUser height " + height;
            Console.WriteLine(information);
        }
    }
}
