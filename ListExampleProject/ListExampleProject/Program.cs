﻿// See https://aka.ms/new-console-template for more information
List<string> tacoShoppingList = new List<string>();
tacoShoppingList.Add("Cinco tacos de suadero");
tacoShoppingList.Add("Cuatro tacos de tripa");
tacoShoppingList.Add("Cinco tacos de pastor");
tacoShoppingList.Add("Cuatro coca colas");

for (int i = 0; i < tacoShoppingList.Count; i++)
{
    Console.WriteLine(tacoShoppingList[i]);
}

tacoShoppingList.Remove("Cinco tacos de suadero"); //Remueve de acuerdo a la cadena de caractares

Console.WriteLine("----------------------------");

for (int i = 0; i < tacoShoppingList.Count; i++)
{
    Console.WriteLine(tacoShoppingList[i]);
}

Console.WriteLine("----------------------------");

tacoShoppingList.RemoveAt(2); // Remueve de la lista declarando el indice
for (int i = 0; i < tacoShoppingList.Count; i++)
{
    Console.WriteLine(tacoShoppingList[i]);
}