﻿using System;

namespace squareArea
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Rectangle area calculation
            Console.WriteLine("Please, input the rectangle side A, you can use decimal (Example: 2,5)");
            float sideA = float.Parse(Console.ReadLine());

            Console.WriteLine("Please, input the rectangle side B, you can use decimal (Example: 2,5)");
            float sideB =  float.Parse(Console.ReadLine());

            // Rectangle area formula is a * b
            float area = sideA * sideB;

            Console.WriteLine("The rectangle Area is: "+ area);
        }
    }
}
