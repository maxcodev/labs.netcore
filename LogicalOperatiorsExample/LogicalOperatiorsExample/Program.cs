﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

/*bool value1 = true;
bool value2 = true;
bool value3 = false;

// &&, ||, !
bool result = value1 && value2 && value3;
Console.WriteLine("The result of the logical AND is: " + result);

bool result2 = value1 || value2 || value3;
Console.WriteLine("The result of the logical NOT is: " + result2);

bool result3 = !value3;
Console.WriteLine("The result of the logical ! is: " + result3);*/

int value1 = 18;
int value2 = 18;
int value3 = -18;

bool result0 = value1 == value2;
Console.WriteLine("The result of value1 == value2 is: " + result0);
bool result1 = value1 != value2;
Console.WriteLine("The result of value1 != value2 is: " + result1);
bool result2 = value1 > value2;
Console.WriteLine("The result of value1 > value2 is: " + result2);
bool result3 = value1 < value2;
Console.WriteLine("The result of value1 < value2 is: " + result3);
bool result4 = value1 >= value2;
Console.WriteLine("The result of value1 >= value2 is: " + result4);
bool result5 = value1 <= value2;
Console.WriteLine("The result of value1 <= value2 is: " + result5);

/*
 == Igual a
 != No igual a
 > Mayor que
 < Menor que
 >= Mayor o igual que
 <= Menor o igual que
 */